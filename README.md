# rasterized_urau_rural


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

This dataset is used in a CM to identify each commuting area around smaller European cities. See the [isochrones repositories](https://gitlab.com/hotmaps/rasterized_iso_rural) for more details. 

## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.





